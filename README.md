Esse projeto foi gerado inicialmente utilizando o [Create React App](https://github.com/facebook/create-react-app).

## Instalação

Primeiro execute o comando `yarn` para instalar os modulos necessários

## Scripts Disponíveis

Neste projeto, você pode rodar:

### `yarn start`

Executa o app in modo desenvolvimento. Abra [http://localhost:3000](http://localhost:3000) para ver no navegador.

A pagina irá recarregar quando você modificar algo. Você também irá ver error de lint no console.

### `yarn build`

Compila o app na pasta `build`, para rodar em produção.

## Mais informações

Este webapp:

- Foi criado baseado na filosofia: "Fazer funcionar, depois melhorar"
- Foi escrito em typescript
- Utiliza React
- Utiliza mobx para gerenciamento de estado
- Utiliza react-router para rotas
- Não é um pwa, mas parte da configuração esta feita
- Utiliza sass
- O layout é estruturado utilizando css flex e grid
- Utiliza modulos css para components e paginas
- É responsivo
- Algumas espaçamentos e tamanho de fontes são diferentes do layout apresentado na descrição do teste. Modifiquei para manter uma melhor consistencia.
- etc

## Features adicionais

- É posivel compartilhar uma buscar utilizando a url
- Generos são clicáveis e levam para uma busca por filmes daquele genero
- Abaixo do formulário de pesquisa é mostrado algumas mensagens sobre a busca