import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Movie from "./pages/Movie";
import Search from "./pages/Search";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path={["/search/:query/page/:pageNumber"]}>
            <Search />
          </Route>
          <Route path="/movie/:id">
            <Movie />
          </Route>
          <Route path="/">
            <Search />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}