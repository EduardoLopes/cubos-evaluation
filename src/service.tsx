const baseURL = "https://api.themoviedb.org/3";
const ApiKey = "0b93b864bef81819a092f443708a49d2";

export type TMovie = {
	id: number;
	title: string;
	original_title: string;
	poster_path: string;
	adult: boolean;
	overview: string;
	release_date: string;
	genre_ids: number[];
	original_language: string;
	backdrop_path: string;
	popularity: number;
	vote_count: number;
	video: boolean;
	vote_average: number;
}

export type TCollection = {
	id: number;
	backdrop_path: string;
	name: string;
	poster_path: string;
}

export type TCompany = {
	id: number;
	logo_path: string;
	name: string;
}

export type TCountry = {
	iso_3166_1: string;
	name: string;
}

export type TLanguage = {
	iso_639_1: string;
	name: string;
}

export type TVideo = {
  id: string;
  iso_639_1: string;
  iso_3166_1: string;
  key: string;
  name: string;
  site: string;
  size: number
  type: string;
}

export type TMovieDetails = TMovie & {
	belongs_to_collection: TCollection;
	budget: number;
	genres: TGenre[];
	homepage: string;
	imdb_id: string;
	production_companies: TCompany[];
	production_countries: TCountry[];
	revenue: number;
	runtime: number;
	spoken_languages: TLanguage[];
  status: string;
  videos: {results: TVideo[]};
	tagline: string;
}

export type TGenre = {
  id: number,
  name: string
}

export type TMovieResult = {
  page: number;
  results: TMovie[];
  total_pages: number;
  total_results: number;
}

export type TGenreResult = {
  genres: TGenre[]
}

export async function checkStatus(res: { json: () => any; status: number; }): Promise<any>{
  const parsed = await res.json();
  const status = res.status;

  if (status >= 200 && status < 300) {
    return parsed;
  }

  return Promise.reject(parsed.errors);
}

export function fetchMovies(query: string, page: number, done: (data: TMovieResult) => void, error: (data: string[]) => void){

  fetch(`${baseURL}/search/movie?api_key=${ApiKey}&language=pt-BR&query=${encodeURI(query)}&page=${page}&include_adult=false`)
  .then(checkStatus)
  .then(
    (result) => {
      done(result);

    },
    (errors) => {
      error(errors);
    }
  );

}

export function fetchMovie(id: number, done: (data: TMovieDetails) => void, error: (data: string[]) => void){

  fetch(`${baseURL}/movie/${id}?api_key=${ApiKey}&language=pt-BR&append_to_response=videos`)
  .then(checkStatus)
  .then(
    (result) => {
      done(result);

    },
    (errors) => {
      error(errors);
    }
  );

}

export function fetchMoviesByGenre(genre: number, page: number, done: (data: TMovieResult) => void, error: (data: string[]) => void){

  fetch(`${baseURL}/discover/movie?api_key=${ApiKey}&language=pt-BR&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}&with_genres=${genre}`)
  .then(checkStatus)
  .then(
    (result) => {
      done(result);

    },
    (errors) => {
      error(errors);
    }
  );

}

export function fetchGenres(done: (data: TGenreResult) => void, error: (data: string[]) => void){

  fetch(`${baseURL}/genre/movie/list?api_key=${ApiKey}&language=pt-BR`)
  .then(checkStatus)
  .then(
    (result) => {
      done(result);

    },
    (errors) => {
      error(errors);
    }
  );

}