/* eslint-disable react-hooks/exhaustive-deps */

import React, { useContext, useEffect } from 'react';
import SearchBar from '../components/SearchBar';
import Movie from '../components/Movie';
import Header from '../components/Header';
import styles from './Search.module.scss';


import AppStoreContext from '../AppStoreContext';
import { observer } from "mobx-react-lite";
import { TMovie } from '../service';
import Pagination from '../components/Pagination';
import { useParams, useHistory } from "react-router-dom";
const Search = observer(() => {

  const store = useContext(AppStoreContext);

  let { query, pageNumber } = useParams<{query: string, pageNumber: string}>();
  let history = useHistory();


  useEffect(() => {

    if(query){

      store.setSearchQuery(query);
      store.setPage(Math.max(0, parseInt(pageNumber) - 1));

    } else {

      store.setSearchQuery("");
      history.push(`/`);

    }


  }, [query, pageNumber]);

  useEffect(() => {

    store.getGenres();

  }, []);

  useEffect(() => {

    if(store.genres.length > 0){

      if(store.searchQuery?.length > 0){
        store.getMovies();
      }

      if(store.searchQuery?.length === 0){
        store.clearMovies();
      }

    }


  }, [store.searchQuery, store.genres]);

  const page = store.page % 4;
  const start = page * store.moviesPerPage;
  const end = (page + 1) * store.moviesPerPage;

  const movies = store.movies?.slice(start, end).map((movie: TMovie, index: number)=> (
    <Movie
      key={index}
      id={movie.id}
      title={movie.title}
      release_date={movie.release_date}
      overview={movie.overview}
      genre_ids={movie.genre_ids}
      vote_average={movie.vote_average}
      poster_path={movie.poster_path}
    />
  ));

  function hangleSearchChange(value: string){
    const p: number = value !== store.searchQuery ? 0 : store.page;

    history.push(`/search/${value}/page/${p + 1}`)

  }

  return (
    <div className="App">
      <Header />
      <div className="container">
        <div className={styles["search-bar-container"]}>
          <SearchBar onChange={hangleSearchChange} />
        </div>

        {store.searchQuery.length !== 0 && <div className={styles.messages}>
          {(store.searchQuery && store.isLoading) && (<div>Carregando...</div>)}
          {store.isError && (<div> ERROR! </div>)}
          {(store.totalResults === 0 && store.searchQuery?.length > 0 && store.isLoading === false) && (<div> Nenhum filme encontrado com a palavra: <strong>{store.searchQuery}</strong> </div>)}

          {( store.totalResults >= 1 && store.isLoading === false) &&
            <div>
              Foram encontrados <strong>{store.totalResults}</strong> filmes {store.isGenre(store.searchQuery) ? "do genero" : "com a palavra"}: <strong>{store.searchQuery}</strong>
            </div>
          }

        </div>}

        {movies}

        <Pagination />

      </div>
    </div>
  );
});

export default Search;
