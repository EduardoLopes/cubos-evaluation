/* eslint-disable react-hooks/exhaustive-deps */

import React, { useContext, useEffect } from 'react';
import Header from "../components/Header";
import { useParams } from "react-router-dom";
import AppStoreContext from '../AppStoreContext';
import Translation from '../Translation';
import { observer } from 'mobx-react-lite';
import styles from './Movie.module.scss';
import { TLanguage, TGenre, TVideo } from '../service';
import Score from '../components/Score';
import GenreList from '../components/GenreList';
import { formatDate } from '../Common';

const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

const minToHHMM = (min: number): string => {

  var hours = Math.floor(min / 60);
  var minutes = min % 60;

  if(hours > 0){
    return hours + 'h ' + minutes + 'min';
  }

  return minutes + 'min';

};

const Detail = (props: {title: string, children: React.ReactNode}) => {

  return (
    <div>
      <h3>{props.title}</h3>
      { props.children }
    </div>
  );

};

const Movie = observer(() => {

  const store = useContext(AppStoreContext);
  let { id } = useParams<{id: string}>();

  useEffect(()=>{

    store.getMovie(parseInt(id));
    window.scrollTo(0, 0);

  }, []);

  const {
    title,
    release_date,
    poster_path,
    overview,
    status,
    spoken_languages,
    runtime,
    budget,
    revenue,
    genres,
    videos,
    vote_average
  } = store.movieDetails;

  const languages = spoken_languages?.map((language: TLanguage, index: number) => {
    return <span key={index}>{ Translation.language[language.name] ? Translation.language[language.name] : language.name}</span>
  });

  function handleLanguagePlural(languages: TLanguage[]){
    return languages?.length > 1 ? "Idiomas" : "Idioma";
  }

  return (
    <div>
      <Header />
      <div className={`container ${styles.movie}`} style={{opacity: store.isLoading ? 0 : 1}}>
        <div className={ styles.topbar }>
          <h2>{ title }</h2>
          <div className={styles['release-date']}>{formatDate(release_date)}</div>
        </div>
        <div className={styles.info}>

          <div className={styles.overview}>
          {overview && <h3>Sinopse</h3>}
            { overview }
          </div>

          <div className={styles.details}>
            <h3>Informações</h3>

            <div className={styles['details-list']}>
              <Detail title="Situalção">{ Translation.status[status] }</Detail>
              {spoken_languages?.length > 0 && <Detail title={handleLanguagePlural(spoken_languages)}>{ languages }</Detail>}
              {runtime > 0 && <Detail title="Duração">{ minToHHMM(runtime) }</Detail>}
              {budget > 0 && <Detail title="Orçamento">{ formatter.format(budget) }</Detail>}
              {revenue > 0 && <Detail title="Receita">{ formatter.format(revenue) }</Detail>}
              {revenue > 0 && <Detail title="Lucro">{ formatter.format(revenue - budget) }</Detail>}
            </div>

            <div className={styles.footer}>

              <div className={styles['genres-container']}>
                {genres && <GenreList names={genres.map((genre: TGenre) => (genre.name))} />}
              </div>

              <div className={styles['score-container']}>
                <Score size="large">{vote_average * 10}%</Score>
              </div>

            </div>

          </div>

        </div>
        <div className={styles.poster}>
          {!poster_path && <div className={styles['no-poster']}>No Poster</div>}
          {poster_path && <img src={`https://image.tmdb.org/t/p/w500/${poster_path}`} alt={`poster do filme ${title}`} />}
        </div>
        <div className={styles.trailer} style={{}}>
          {videos?.results.length > 0 && <div className={styles.wraper}>
            {videos.results.map((video: TVideo)=> video.site === "YouTube" ? <iframe key={video.id} width="560" height="315" title="Trailer" src={`https://www.youtube.com/embed/${video.key}`} frameBorder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /> : null)}
          </div>}
        </div>
      </div>
    </div>
  );

});

export default Movie;