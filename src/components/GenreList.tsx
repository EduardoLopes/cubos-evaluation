import React from 'react';
import styles from './GenreList.module.scss';
import { Link } from 'react-router-dom';

type Props = {
  names: string[];
}

export default function GenreList(props: Props){

  const genres = props.names.map((name: string, index: number) => (
    <li key={index}>
      <Link to={`/search/${name}/page/1`}>{name}</Link>
    </li>
  ));

  return(
    <ul className={styles.genres}>
      { genres }
    </ul>
  )
}