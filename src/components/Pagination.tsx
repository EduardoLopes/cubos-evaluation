import React, { useContext } from 'react';
import { observer } from "mobx-react-lite";
import styles from './Pagination.module.scss';
import ReactPaginate from 'react-paginate';
import AppStoreContext from '../AppStoreContext';
import { useHistory } from "react-router-dom";


const Pagination = observer(() => {
  const store = useContext(AppStoreContext);
  let history = useHistory();

  const totalPages = Math.ceil(store.totalResults / store.moviesPerPage);

  return(
    <ReactPaginate
      breakLabel={'...'}
      breakClassName={styles['break-me']}
      pageCount={totalPages}
      forcePage={store.page}
      marginPagesDisplayed={2}
      pageRangeDisplayed={3}
      onPageChange={(data)=> (history.push(`/search/${store.searchQuery}/page/${data.selected + 1}`))}
      containerClassName={styles.pagination}
      activeClassName={styles.active}
      previousClassName={styles.previous}
      nextClassName={styles.next}
    />
  )
});

export default Pagination;