import React from 'react';
import styles from './Score.module.scss';

export default function Score(props: {children: React.ReactNode, size?: string}){
  return(
    <div className={`${styles.score} ${props.size ? styles[props.size] : ''}`}>
      { props.children }
    </div>
  )
}