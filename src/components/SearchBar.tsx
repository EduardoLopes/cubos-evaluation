/* eslint-disable react-hooks/exhaustive-deps */

import React, { useEffect, useState} from 'react';
import { observer } from "mobx-react-lite";
import styles from './SearchBar.module.scss';
import { useParams } from "react-router-dom";

type Props = {
  onChange: (value: string) => void
}

const SearchBar = observer((props: Props) => {

  let { query } = useParams<{query: string}>();
  const [inputValue, setInputValue] = useState<string>('');

  useEffect(() => {

    if(query){
      setInputValue(query)
    }

  }, []);

  const { onChange } = props;
  let intervalID : NodeJS.Timeout;

  function handleOnChange(event: React.ChangeEvent<HTMLInputElement>){

    event.persist();
    setInputValue(event.target.value);
    clearTimeout(intervalID)
    intervalID = setTimeout(()=> onChange(event.target.value), 500);

  }

  // const genres = store.genres.map((genre: TGenre, index: number)=>(
  //   <li key={index}><a href="#">{genre.name}</a></li>
  // ));

  return (
    <section role="search" className={styles.search}>
      <form>
        <fieldset>
          <label htmlFor="search" className="sr-only">Busque um file por nome ou genero:</label>
          <input value={inputValue} onChange={handleOnChange} type="search" id="search" name="search" placeholder="Busque um file por nome ou genero" />
            {/* Generos: <ul className={styles.suggestions}>
              {genres}
            </ul> */}

        </fieldset>
      </form>
    </section>
  );

});

export default SearchBar;
