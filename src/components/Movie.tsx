import React, { useContext } from 'react';
import styles from './Movie.module.scss';
import { Link } from "react-router-dom";
import AppStoreContext from '../AppStoreContext';
import Score from './Score';
import GenreList from './GenreList';
import { formatDate } from '../Common';

type Props = {
  id: number;
  title: string;
  release_date: string,
  overview: string,
  genre_ids: number[],
  vote_average: number,
  poster_path: string
}

function Movie(props : Props) {

  const { title, id, release_date, overview, genre_ids, vote_average, poster_path } = props;
  const store = useContext(AppStoreContext);

  const genres = genre_ids.map((id) => (
    <li key={id}>
      {store.getGenre(id)?.name}
    </li>
  ));

  return (
    <div className={styles.movie}>
      <div className={styles.poster}>
        {!poster_path && <div className={styles['no-poster']}>No Poster</div>}
        {poster_path && <img src={`https://image.tmdb.org/t/p/w500/${poster_path}`} alt={`poster do filme ${title}`} />}
      </div>
      <div className={styles.info}>
        <div className={styles.title}>
          <div className={styles['score-container']}>
            <Score>{vote_average * 10}%</Score>
          </div>
          <h2>
            <Link to={`/movie/${id}`}>{ title }</Link>
          </h2>
        </div>

        <div className={styles['release-date']}>{formatDate(release_date)}</div>
        <div className={styles.overview}>{overview}</div>

        <div className={styles['genre-container']}>
          {genres && <GenreList names={genre_ids.map((id: number) => (store.getGenre(id)?.name))} />}
        </div>

      </div>
    </div>
  );
}

export default Movie;
