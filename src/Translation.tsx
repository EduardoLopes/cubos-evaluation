// A simple method of translation

export default {
  status: {
    "Rumored": "Rumores",
    "Planned": "Planejado",
    "In Production": "Em Produção",
    "Post Production": "Pós-produção",
    "Released": "Lançado",
    "Canceled": "Cancelado"
  } as {[key: string]: string},
  // this is some of the languages i saw the most, no time to translate all the languages hah :)
  language: {
    "English": "Inglês",
    "Deutsch": "Alemão",
    "Nederlands": "Holandês",
    "Český": "Tcheco"
  } as {[key: string]: string}
};