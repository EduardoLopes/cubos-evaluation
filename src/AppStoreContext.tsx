import { createContext } from 'react';
import { fetchMovies, fetchMoviesByGenre, fetchGenres, fetchMovie, TMovieResult, TGenreResult, TMovie, TGenre, TMovieDetails } from './service';

export const createStore = () => ({
  searchQuery: '',
  movies: [] as TMovie[],
  page: 0,
  totalPages: 0,
  totalResults: 0,
  genres: [] as TGenre[],
  isLoading: false,
  isError: false,
  errorMessages: [] as string[],
  moviesPerPage: 5,
  movieDetails: {} as TMovieDetails,
  setSearchQuery(value: string){
    if(!value || value?.length === 0){
      this.setTotalPages(0);
      this.setTotalResults(0);
    }

    if(this.searchQuery !== value){
      this.clearMovies();
    }

    this.searchQuery = value;
  },
  setMovies(value: TMovie[]){
    this.movies = value;
  },
  setMovieDetail(value: TMovieDetails){
    this.movieDetails = value;
  },
  setGenres(value: TGenre[]){
    this.genres = value;
  },
  setErrorMessages(value: string[]){
    this.errorMessages = value;
  },
  setIsError(value: boolean){
    this.isError = value;
  },
  setIsLoading(value: boolean){
    this.isLoading = value;
  },
  setPage(value: number){

    window.scrollTo(0, 0);

    if(Math.floor((this.page * this.moviesPerPage) / 20) !== Math.floor((value * this.moviesPerPage) / 20)){
      this.page = value;
      this.getMovies();
      return;
    }

    this.page = value;

  },
  setTotalPages(value: number){
    this.totalPages = value;
  },
  setTotalResults(value: number){
    this.totalResults = value;
  },
  isGenre(name: string): boolean{
    const genre = this.genres.filter((genre)=> genre.name === name)
    return genre.length > 0
  },
  getGenreByName(name: string): TGenre[]{
    return this.genres.filter((genre: TGenre)=> genre.name === name)
  },
  getGenre(id: number): TGenre {
    const genre = this.genres.filter((genre)=> genre.id === id)
    return genre[0]
  },
  getGenres(){

    fetchGenres((result: TGenreResult)=>{

      this.setGenres(result.genres);

    },
    (errors: string[]) => {
      this.setIsError(true);
      this.setIsLoading(false);
      this.setMovies([]);
      this.setErrorMessages(errors);
    });

  },
  getMovie(id: number){

    this.setIsLoading(true);

    const done = (result: TMovieDetails)=>{
      this.setIsError(false);
      this.setIsLoading(false);
      this.setMovieDetail(result);
    };

    const error = (errors: string[]) => {
      this.setIsError(true);
      this.setIsLoading(false);
      this.setErrorMessages(errors);
    };

    fetchMovie(id, done, error);

  },
  getMovies(){

    this.setIsLoading(true);
    this.clearMovies();

    const done = (result: TMovieResult)=>{
      this.setIsError(false);
      this.setIsLoading(false);
      this.setMovies(result.results);
      this.setTotalPages(result.total_pages);
      this.setTotalResults(result.total_results);
    };

    const error = (errors: string[]) => {
      this.setIsError(true);
      this.setIsLoading(false);
      this.setMovies([]);
      this.setErrorMessages(errors);
    };

    if(this.isGenre(this.searchQuery)){
      const genre: TGenre = this.getGenreByName(this.searchQuery)[0];
      fetchMoviesByGenre(genre.id, Math.floor((this.page * this.moviesPerPage) / 20) + 1, done, error);
    } else {
      fetchMovies(this.searchQuery, Math.floor((this.page * this.moviesPerPage) / 20) + 1, done, error);
    }

  },
  clearMovies(){
    this.movies.length = 0;
  }
});

export type TStore = ReturnType<typeof createStore>;

export default createContext<TStore>(createStore());